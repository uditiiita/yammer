<?php

include_once 'utils.php';

$messages = select_from_db("*", "messages", "", "");
$output = array();
$output['messages'] = array();
$j = 0;
for ( $i = 0; $i < count($messages); $i++ ) {
    $singleMessage = $messages[$i];
    
    $outputMessage = array();
    $outputMessage['sender_id'] = $singleMessage['senderID'];
    $outputMessage['body']['plain'] = decodeFromDB($singleMessage['body']);
    if(trim($outputMessage['body']['plain']) == ''){
        continue;
    }
    if($singleMessage['contentClass'] == 'Image') {
        $outputMessage['attachments'][0]['content_class'] = $singleMessage['contentClass'];
        $largePreviewURL = $singleMessage['largePreviewURL'];
        if($largePreviewURL){
            $outputMessage['attachments'][0]['large_preview_url'] = $largePreviewURL;
        }
    }
    //var_dump($outputMessage);
    //echo "<br><br>";
    $output['messages'][$j++] = $outputMessage;
}

echo json_encode($output);
//$num = 

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
