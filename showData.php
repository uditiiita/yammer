<?php
include_once 'utils.php';

$messages = select_from_db("*", "messages", "", "");

for ( $i = 0; $i < count($messages); $i++ ) {
    $singleMessage = $messages[$i];
    
    $senderID = $singleMessage['senderID'];
    $senderName = $singleMessage['senderName'];
    $senderJobTitle = $singleMessage['senderJobTitle'];
    $senderProfileURL = $singleMessage['senderProfileURL'];
    $senderProfileURL = str_replace('{width}', "250", $senderProfileURL);
    $senderProfileURL = str_replace('{height}', "250", $senderProfileURL);
    $body = decodeFromDB($singleMessage['body']);
    $contentClass = $singleMessage['contentClass'];
    $largePreviewURL = $singleMessage['largePreviewURL'];
    echo "<br><br>";
    echo "Sender ID: $senderID <br>";
    echo "Sender: $senderName ( $senderJobTitle ) <br>";
    echo "Profile Picture:<br> <img src='$senderProfileURL'> <br>";
    echo "Body: $body <br>";
    
    if($largePreviewURL && $contentClass=="Image"){
        echo "Image: <br> <img src='$largePreviewURL' style='max-height: 500px; max-width: 500px;'> <br><Br>";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
