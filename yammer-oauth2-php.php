<?php
include_once 'utils.php';
include_once 'yammerCaptureClass.php';

function getFileNameFromFullURL($fullURL){
    return urldecode(basename($fullURL));
}

function fetchImageAndSaveItLocally($yammer, $remoteURL, $localPath, $imageName=NULL){
    if ($imageName==NULL) {
        $imageName = getFileNameFromFullURL($remoteURL);
    }
    $imageData = $yammer->getImageData($remoteURL);
    $localFilePath = $localPath."/".$imageName;
    saveImage($localFilePath, $imageData);
    return $localFilePath;
}

function createPathIfNotExists($filePath){
    $pathComponents = explode('/', $filePath);
    $depth = count($pathComponents)-1;
    $currentPath = ".";
    for($i = 0; $i < $depth; $i++){
        $currentPath = $currentPath."/".$pathComponents[$i];
        if(!file_exists($currentPath)){
            mkdir($currentPath, 0777, true);
        }
    }
}

function saveImage($filePath, $imageData) {
    createPathIfNotExists($filePath);
    file_put_contents($filePath, $imageData);
}

function insertMessageIntoDB($message, $yammer){
    $senderID = $message->sender_id;
    
    $userInfo = $yammer->get("users/$senderID.json");
    $body = encodeForDB($message->body->plain);
    $senderName = $userInfo->full_name;
    $senderProfileURL = $userInfo->mugshot_url_template;
    
    echo "<br>";
    var_dump($userInfo);
    echo "<br>Sender ID: ". $senderID;
    echo "<br>Sender Name: ". $senderName;
    echo "<br>Sender Profile: ". $senderProfileURL;
    $senderJobTitle = $userInfo->job_title;
    $contentClass = $ContentClassNotFound;
    $largePreviewURL = "";
    $thumbnailURL = "";
    if ($message->attachments && count($message->attachments)>0) {
        $attachment = $message->attachments[0];
        $contentClass = $attachment->content_class;
        $largePreviewURL = $attachment->large_preview_url;
        $thumbnailURL = $attachment->thumbnail_url;
        
        if($largePreviewURL && $contentClass=="Image"){
            $largePreviewURL = fetchImageAndSaveItLocally($yammer, $largePreviewURL, "images/large-preview");
            echo "<br>".$largePreviewURL;
        }
        
        if($thumbnailURL){
            //$thumbnailURL = fetchImageAndSaveItLocally($yammer, $thumbnailURL, "images/thumbnail");
        }
    }
    
    
    
    $insertData = array(
        "senderID" => $senderID,
        "senderName" => $senderName,
        "senderProfileURL" => $senderProfileURL,
        "senderJobTitle" => $senderJobTitle,
        "body" => $body,
        "contentClass" => $contentClass,
        "largePreviewURL" => $largePreviewURL,
        "thumbnailURL" => $thumbnailURL
    );
    
    insert_into_db($insertData, "messages");
    echo "<br>";
}

$config['consumer_key'] = 'TNtM5xr86YQa2JoOpO8w';
$config['consumer_secret']   = 'RjWcHlA5sF1ZXuGfLdQCmBraw4h9YIhvmXoi3QkLjW8';
$config['callbackUrl']  = 'http://' . $_SERVER['SERVER_NAME'] . '/yammer-oauth2-php.php';
if(isDevelopmentEnvironment()){
    $config['callbackUrl']  = 'http://' . $_SERVER['SERVER_NAME'] . '/freelancing/yammer-periodic-fetch/yammer-oauth2-php.php';
}

$yammer = new YammerPHP($config);

try{
    $accessToken = $predefinedAccessToken;

    $yammer->setOAuthToken($accessToken);

    $messagesData = $yammer->get("messages.json?threaded=true");

    if($messagesData) {
        //echo "success";
        $messages = $messagesData->messages;
        runQuery("delete from messages where 1");
        echo "Deleted old messages<br>";

        for($i = 0; $i < count($messages); $i++){
            echo "<br>Inserting Message: $i";
            $message = $messages[$i];
            insertMessageIntoDB($message, $yammer);
        }
        echo "Inserted new messages<br>";
    } else {
        echo "error<br>";
        echo "<br><br>Invalid Access token: <br>".$yammer->getAuthorizationUrl()."<br>";
        exit();
    }

} catch (Exception $e) {
    echo "exception";
    var_dump($e);
    echo "<br><br>Invalid Access token: <br>".$yammer->getAuthorizationUrl()."<br>";
    exit();
}

?>
