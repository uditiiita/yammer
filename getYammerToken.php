<?php
include_once 'utils.php';
/**
 * Yammer OAuth2 Class
 *
 * Example configuration array passed to constructor:
 *
 *    $config['consumer_key'] = '1ABCdefhiJKLmnop';
 *    $config['consumer_secret']   = 'ABCdefhi_JKLmnop';
 *    $config['callbackUrl']  = 'http://' . $_SERVER['SERVER_NAME'] . '/yammer/callback/';
 *
 *     $yammer = new YammerPHP($config);
 */
class YammerPHP {

	public $consumerKey;
	public $consumerSecret;
	public $oauthToken;
	public $oauthTokenSecret;
	public $callbackUrl;

	protected $authToken;

	/**
	 * Class Constructor
	 *
	 * @param array $config 
	 */
	function __construct($config) {
		$this->consumerKey = $config['consumer_key'];
		$this->consumerSecret = $config['consumer_secret'];
		$this->callbackUrl = $config['callbackUrl'];

		/* Set Up OAuth Consumer */
		if (isset($config['oauth_token']) && $config['oauth_token_secret']):
			$this->oauthToken = $config['oauth_token'];
			$this->oauthTokenSecret = $config['oauth_token_secret'];
		endif;
	}

	/**
	 * Get Authorization Url
	 *
	 * @param string $callbackUrl 
	 * @return $url
	 */
	function getAuthorizationUrl($callbackUrl = null) {

		/* Override if needed, else assume it was set at __construct() */
		if ($callbackUrl)
			$this->callbackUrl = $callbackUrl;

		/* Authorization URL */
		$url = sprintf('https://www.yammer.com/dialog/oauth?client_id=%s&redirect_uri=%s',
			$this->consumerKey,
			urlencode($this->callbackUrl)
		);

		return $url;
	}

	/**
	 * Get Access Token
	 *
	 * @param string $code 
	 * @param string $isRefresh 
	 * @return $response
	 */
	function getAccessToken($code = null, $isRefresh = false) {

		$data = array();
		if (!$isRefresh):
			// For Yammer, refresh requests are identical to original requests
			$data['code'] = $code;
			$data['client_id'] = $this->consumerKey;
			$data['client_secret'] = $this->consumerSecret;
		else:
			$data['code'] = $code;
			$data['client_id'] = $this->consumerKey;
			$data['client_secret'] = $this->consumerSecret;
		endif;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://www.yammer.com/oauth2/access_token.json');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);

		if (in_array(curl_getinfo($ch, CURLINFO_HTTP_CODE), array(400,401)) ):
			$t_response = json_decode($response);
			if (isset($t_response->error) && $t_response->error != '')
				$response = $t_response->error;
			throw new YammerPHPException('Server: ' . $response, curl_getinfo($ch, CURLINFO_HTTP_CODE));
		endif;

		$response = json_decode($response);
		return $response;

	}

	/**
	 * Set OAuth token
	 *
	 * @param string $token 
	 */
	function setOAuthToken($token = null) {
		$this->oauthToken = $token;
	}

	/**
	 * Get Resource
	 *
	 * @param string $resource - Path (after ../v0/) of the resource you're accessing
	 * @param array $data - $_GET variables to pass
	 */
	public function get($resource = null, $data = array()) {
		$url = 'https://www.yammer.com/api/v1/' . $resource;
		return $this->request($url, $data);
	}


	/* Helpers */

	/**
	 * Test Authentication
	 *
	 * @return boolean
	 */
	function testAuth() {
		$url = 'https://www.yammer.com/api/v1/messages/following.json';
		try {
			$result = $this->request($url);
			return true;
		} catch (YammerPHPException $e) {
			return false;
		}
	}


	// @todo: Add more helpers


	/* Private request method */

	/**
	 * Request Resource
	 *
	 * @param string $url 
	 * @param array $data 
	 * @return $return
	 */
	public function request($url, $data = array()) {
		$headers = array();
		$headers[] = "Authorization: Bearer " . $this->oauthToken;
                echo "<br>1";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($data) );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		// Throw exception on no response from server
		echo "<br>2";
                echo "<br>output:$output";
                if (!$output)
			throw new YammerPHPException('No response from server.', curl_getinfo($ch, CURLINFO_HTTP_CODE) );
                
		$return = json_decode($output);
                echo "<br>3";
		// Throw an exception on error
		if (isset($return->BODY->H2) && $return->BODY->H2 == 'Error 401')
			throw new YammerPHPException($return->BODY->H1, 401);
                echo "<br>4";
		return $return;
	}
        
        public function getImageData($url, $data = array()) {
		$headers = array();
		$headers[] = "Authorization: Bearer " . $this->oauthToken;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($data) );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		// Throw exception on no response from server
		if (!$output)
			throw new YammerPHPException('No response from server.', curl_getinfo($ch, CURLINFO_HTTP_CODE) );
                return $output;
		//$return = json_decode($output);

		/*// Throw an exception on error
		if (isset($return->BODY->H2) && $return->BODY->H2 == 'Error 401')
			throw new YammerPHPException($return->BODY->H1, 401);

		return $return;*/
	}

}

/**
 * Yammer Exception Class
 */
class YammerPHPException extends Exception {
	public function __construct($message, $code = 0, Exception $previous = null) {
		parent::__construct($message, $code, $previous);
	}
}

function getFileNameFromFullURL($fullURL){
    return urldecode(basename($fullURL));
}

function fetchImageAndSaveItLocally($yammer, $remoteURL, $localPath, $imageName=NULL){
    if ($imageName==NULL) {
        $imageName = getFileNameFromFullURL($remoteURL);
    }
    $imageData = $yammer->getImageData($remoteURL);
    $localFilePath = $localPath."/".$imageName;
    saveImage($localFilePath, $imageData);
    return $localFilePath;
}

function createPathIfNotExists($filePath){
    $pathComponents = explode('/', $filePath);
    $depth = count($pathComponents)-1;
    $currentPath = ".";
    for($i = 0; $i < $depth; $i++){
        $currentPath = $currentPath."/".$pathComponents[$i];
        if(!file_exists($currentPath)){
            mkdir($currentPath, 0777, true);
        }
    }
}

function saveImage($filePath, $imageData) {
    createPathIfNotExists($filePath);
    file_put_contents($filePath, $imageData);
}

function insertMessageIntoDB($message, $yammer){
    $senderID = $message->sender_id;
    
    $userInfo = $yammer->get("users/$senderID.json");
    showMessageForDevelopmentEnvironment($userInfo);
    $body = encodeForDB($message->body->plain);
    $senderName = $userInfo->full_name;
    $senderProfileURL = $userInfo->mugshot_url_template;
    $senderJobTitle = $userInfo->job_title;
    $contentClass = $ContentClassNotFound;
    $largePreviewURL = "";
    $thumbnailURL = "";
    if ($message->attachments && count($message->attachments)>0) {
        $attachment = $message->attachments[0];
        $contentClass = $attachment->content_class;
        $largePreviewURL = $attachment->large_preview_url;
        $thumbnailURL = $attachment->thumbnail_url;
        
        if($largePreviewURL && $contentClass=="Image"){
            $largePreviewURL = fetchImageAndSaveItLocally($yammer, $largePreviewURL, "images/large-preview");
            echo $largePreviewURL."<br>";
        }
        
        if($thumbnailURL){
            //$thumbnailURL = fetchImageAndSaveItLocally($yammer, $thumbnailURL, "images/thumbnail");
        }
    }
    
    
    
    $insertData = array(
        "senderID" => $senderID,
        "senderName" => $senderName,
        "senderProfileURL" => $senderProfileURL,
        "senderJobTitle" => $senderJobTitle,
        "body" => $body,
        "contentClass" => $contentClass,
        "largePreviewURL" => $largePreviewURL,
        "thumbnailURL" => $thumbnailURL
    );
    
    insert_into_db($insertData, "messages");
}

$config['consumer_key'] = 'TNtM5xr86YQa2JoOpO8w';
$config['consumer_secret']   = 'RjWcHlA5sF1ZXuGfLdQCmBraw4h9YIhvmXoi3QkLjW8';
$config['callbackUrl']  = 'http://' . $_SERVER['SERVER_NAME'] . '/getYammerToken.php';
if(isDevelopmentEnvironment()){
    $config['callbackUrl']  = 'http://' . $_SERVER['SERVER_NAME'] . '/freelancing/yammer-periodic-fetch/yammer-oauth2-php.php';
}

$yammer = new YammerPHP($config);
if( isset($_REQUEST['code'])) {
    $code = $_REQUEST['code'];
    
    try{
        $accessTokenResponse = $yammer->getAccessToken($code);
        $accessToken = $accessTokenResponse->access_token->token;
        
        echo "<br>access token: '$accessToken'";

    } catch (Exception $e) {
        echo "exception";
        var_dump($e);
        echo "<br><br>Visit: <br>".$yammer->getAuthorizationUrl()."<br>";
        header("Location: ". $yammer->getAuthorizationUrl());
    }
} else {
    echo "<br><br>Visit: <br>".$yammer->getAuthorizationUrl()."<br>";
    header("Location: ". $yammer->getAuthorizationUrl());
}

?>
