<?php
date_default_timezone_set('Asia/Kolkata'); 
$predefinedAccessToken = "UHVTjmUHYflXNbBO3DGOFg";

$development = false;
if (isset($_SERVER['development']) || isset($_REQUEST['debug']) ) {
    $development = true;
}
if ( !isset($_REQUEST['debug']) && !$development ) {
	//error_reporting(0);
}

function isDevelopmentEnvironment(){
    global $development;
    if(isset($development) && $development) {
        return true;
    }
    return false;
}
include_once 'dbUtils.php';
$ContentClassNotFound = "NF";
function showMessageForDevelopmentEnvironment($msg){
    global $development;
    if(isset($development) && $development) {
        var_dump($msg);
        echo "<br>";
    }
}

?>
