<?php

function getDbConnectionInformation(){
    $sql['server']     = "localhost";
    $sql['user']       = "truefann_udit";
    $sql['password']   = "funwithapps";
    $sql['db']         = "truefann_newrubicast";
    
    if(isDevelopmentEnvironment()){
        $sql['server']     = "localhost";
        $sql['user']       = "root";
        $sql['password']   = "root";
        $sql['db']         = "yammer-offiline-fetch";
    }
    
    return $sql;
}

function connect_db(){
    $sqlInfo = getDbConnectionInformation();
    $conn =  mysql_pconnect( $sqlInfo['server'], $sqlInfo['user'], $sqlInfo['password']  ) or
		die( 'Could not open connection to server'."error: ".mysql_error() );
    mysql_select_db( $sqlInfo['db'], $conn ) or 
		die( 'Could not select database '. $sqlInfo['db']."error: ".mysql_error() );
    return $conn;
}

function generate_insert_query($data, $table_name){

   $query = "insert into " . $table_name. "(";
   $i = 1;
   foreach($data as $key => $value){
   	if($i != 1){
		$query = $query.",";
   	}
	$i = 2;
   	$query = $query.$key;
   }
   $query = $query.") values(";
   
   $i = 1;
   foreach($data as $key => $value){
   	if($i != 1){
		$query = $query.",";
   	}
   	$query = $query.'"'.$value.'"';
   			$i = 2;
   }
   $query = $query.")";
   return $query;
}

function select_from_db_with_conn($columns, $table, $where, $last, $conn) {
	$column_names = "";
	for($i = 0; $i < count($columns);$i++){
		if($i ==0 ){
			$column_names .= $columns[$i];
		} else{
			$column_names .= ",".$columns[$i];
		}
	}

	$query = "SELECT ".$column_names." FROM `".$table."`";
	if(isset($where) && $where != ""){
		$query = $query." WHERE " . $where . " ";
	}
        if(isset($last) && $last != ""){
            $query = $query. " " . $last;
        }
	//echo "Select query: ".$query."<br>";
	$rs = mysql_query($query,$conn);
	$result = array();
        if(!$rs){
            die('Could not fetch result: ' . mysql_error());
            return $result;
        }
	while( $row=mysql_fetch_array($rs)){
		$result[] = $row;
	}
	return $result;
	
}

function select_from_db($columns, $table, $where, $last) {
	$column_names = "";
	for($i = 0; $i < count($columns);$i++){
		if($i ==0 ){
			$column_names .= $columns[$i];
		} else{
			$column_names .= ",".$columns[$i];
		}
	}
	$conn = connect_db();

	$query = "SELECT ".$column_names." FROM ".$table;
	if(isset($where) && $where != ""){
		$query = $query." WHERE " . $where . " ";
	}
        if(isset($last) && $last != ""){
            $query = $query. " " . $last;
        }
	//echo "Select query: ".$query."<br>";
	$rs = mysql_query($query,$conn);
	$result = array();
        if(!$rs){
            die('Could not fetch result: ' . mysql_error());
            return $result;
        }
	while( $row=mysql_fetch_array($rs)){
		$result[] = $row;
	}
        mysql_close($conn);
	return $result;
	
}

function insert_into_db ( $data, $table ) {
	$conn = connect_db();
    	$query = generate_insert_query($data, $table);
        //echo $query;
    	$ret = mysql_query($query,$conn);
        if(!$ret){
            echo 'Could not insert result: ' . mysql_error()."<br>";
        }
        mysql_close($conn);
}
function insert_into_db_with_conn ( $data, $table, $conn ) {
    	$query = generate_insert_query($data, $table);
    	$ret = mysql_query($query,$conn);
        if(!$ret){
            echo 'Could not insert result: ' . mysql_error()."<br>";
        }
}

function insert_into_db_and_return_id ( $data, $table ) {
	$conn = connect_db();
    	$query = generate_insert_query($data, $table);
        //echo "$query";
    	$ret =  mysql_query($query,$conn);
        if(!$ret){
            echo 'Could not insert result: ' . mysql_error(); 
        }
        mysql_close($conn);
        return mysql_insert_id($conn);//Need to get last inserted record's id fron databae
}


function generate_update_query($data, $table_name, $where){
    $query = "UPDATE " . $table_name. " SET ";
    $i = 1;
    foreach($data as $key => $value){
        if($i != 1){
            $query = $query.",";
        }
        $query = $query.$key.'="'.$value.'"';
        $i = 2;
    }
    
    if(isset($where) && $where != ""){
        $query = $query." WHERE " . $where . " ";
    }
    
    return $query;
}


function update_into_db ( $data, $table, $where ) {
	$conn = connect_db();
    	$query = generate_update_query($data, $table, $where);
        //echo $query.'<br>';
    	$ret = mysql_query($query,$conn);
        if(!$ret){
            echo 'Could not update result: ' . mysql_error(); 
        }
        mysql_close($conn);
}

function update_into_db_with_conn ( $data, $table, $where, $conn ) {
    	$query = generate_update_query($data, $table, $where);
        //echo $query.'<br>';
    	$ret = mysql_query($query,$conn);
        if(!$ret){
            echo 'Could not update result: ' . mysql_error(); 
        }
}

function delete_from_db ( $table, $where ) {
	$conn = connect_db();
    	$query = "DELETE FROM $table WHERE $where";
        //echo $query.'<br>';
    	$ret = mysql_query($query,$conn);
        if(!$ret){
            echo 'Could not delete result: ' . mysql_error(); 
        }
        mysql_close($conn);
}

function insert_if_not_present($data, $table, $where) {
    $returnVals = select_from_db("*", $table, $where, "");
    //print_r($returnVals);
    if(count($returnVals) > 0){
        //echo "already present<br>";
    } else {
        //echo "not already present<br>";
        insert_into_db($data, $table);
        //echo "inserted<br>";
    }
}

function insert_if_not_present_with_conn($data, $table, $where, $conn) {
    $returnVals = select_from_db_with_conn("*", $table, $where, "", $conn);
    //print_r($returnVals);
    if(count($returnVals) > 0){
        //echo "already present<br>";
    } else {
        //echo "not already present<br>";
        insert_into_db_with_conn($data, $table, $conn);
        //echo "inserted<br>";
    }
}

function insert_or_update($data, $table, $where) {
    $returnVals = select_from_db("*", $table, $where, "");
    //print_r($returnVals);
    if(count($returnVals) > 0){
        update_into_db($data, $table, $where);
    } else {
        //echo "not already present<br>";
        insert_into_db($data, $table);
        //echo "inserted<br>";
    }
}

function insert_or_update_with_conn($data, $table, $where, $conn) {
    $returnVals = select_from_db_with_conn("*", $table, $where, "", $conn);
    //print_r($returnVals);
    if(count($returnVals) > 0){
        update_into_db_with_conn($data, $table, $where, $conn);
    } else {
        //echo "not already present<br>";
        insert_into_db_with_conn($data, $table, $conn);
        //echo "inserted<br>";
    }
}


function getAttractionForFBUserId($ufbid) {
    $resultVals = select_from_db("*", "users", "fbid='$ufbid'", "limit 1");
    //print_r($resultVals);
    return $resultVals[0]['attraction'];
}

function create_table_if_not_exists($columns, $table) {
    $conn = connect_db();
    
    $query= "CREATE TABLE IF NOT EXISTS `$table` (";
    $i = 1;
    foreach($columns as $name => $type){
   	if($i != 1){
		$query = $query.",";
   	}
	$i = 2;
   	$query = $query."`$name` $type";
    }
    $query = $query.") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    //echo "create table query: $query";
    $ret = mysql_query($query,$conn);
    if(!$ret){
        echo 'Could not create table: ' . mysql_error(); 
    }
    mysql_close($conn);
}

function runQuery($query){
    $conn = connect_db();
    $ret = mysql_query($query,$conn);
    if(!$ret){
        showMessageForDevelopmentEnvironment( "Could not run query: {$query}, error: {" . mysql_error()."}" ); 
    }
    mysql_close($conn);
}

function encodeForDB($plainData){
    $jsonEncoded = json_encode($plainData);
    $base64Encoded = base64_encode($jsonEncoded);
    return $base64Encoded;
}

function decodeFromDB($encodedData){
    $jsonEncoded = base64_decode($encodedData);
    $plainData = json_decode($jsonEncoded);
    return $plainData;
}

?>
