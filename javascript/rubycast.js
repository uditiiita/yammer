$(function() {
    var whichnum = -1; 
    var interval = 20000, 
        handle = null;
    var current = null;
    var container = $("#rollingMessage");

    function incrementWhichNum(){
        ++whichnum;
        if(whichnum >= 25 ) { 
            whichnum = 0;
        }
    }

    function prepMessage( data ){
            var obj = JSON.parse( data );

            if( $.isArray( obj.messages )  && obj.messages.length > 0 ){
                
                while(whichnum >= obj.messages.length){
                    incrementWhichNum();
                }
                current = obj.messages[whichnum];
                console.log(whichnum);
                console.log(current);
                $.get('userinfo.php?sender_id=' + current['sender_id'], renderScreen);
            }
    }

    function replaceURLWithValue(url,value) {
        var replace_with = value ? value : '';
        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return url.replace(exp, replace_with);
    }
    
    function renderScreen( data ){
        var userdata = JSON.parse(data);
        var body = current.body.plain;
            body = replaceURLWithValue(body, ''); // I can put any content to replace where the URL use to be between the apostrophes to the left
            
        if(body.trim() == ''){
            console.log("Found blank body. Rolling again");
            rollScreen();
            return;
        }
        container.slideUp('slow',function(){
            container.html('');

            var mugshot = String( userdata.mugshot_url_template );
            mugshot = mugshot.replace(/{width}/, '250');
            mugshot = mugshot.replace(/{height}/, '250');

            if( current.body.plain.search("#newteammember") != -1 ) {					
                    body = body.substring(0, 100);
                    body += '... <span= style=\"color:#ea113a;\">continued on Yammer.</span>';
            } else {
                body = body.substring(0, 200);
                body += '... <span= style=\"color:#ea113a;\">continued on Yammer.</span>';
            }

            var ranValue = Math.floor(Math.random()*8);

            img = $('<div>').addClass('imageContainer').css('background-image','url(' + mugshot +')');
            headerContainer = ("<div class=\"headerContainer\"> <h1>" + userdata.full_name + "</h1><h2> " + userdata.job_title + " </h2> </div>");
            figure = $("<div>").addClass('figure').append(img).append(headerContainer);
            container.append(figure).append("<div class=\"bodyContainer\"><h3>" + body + '</h3><br clear=\"all\"></div>');
            try{
                if( current.attachments && current.attachments[0] ){
                    var bc = container.find('.bodyContainer');
                    if( current.attachments[0].large_preview_url ) {
                        bc.append($('<img>').addClass('imageRight').attr('src', current.attachments[0].large_preview_url ));
                    }
                    else if( current.attachments[0].thumbnail_url ) {
                        bc.append($('<img>').addClass('imageRight').attr('src', current.attachments[0].thumbnail_url));
                    }
                }
            }catch(e){
                console.log('unexpected error', e);
            }
            container.slideDown('slow');
        }); 	
    }

    function rollScreen(){
        $.get('rubyproxy.php', prepMessage );
        incrementWhichNum();
    }

    rollScreen();
    handle = setInterval( rollScreen, interval );	

});