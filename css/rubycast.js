$(function() {
    var whichnum = 0;
    var start = 0, 
    max = 25,
    interval = 2000,
    handle = null,
    maxChars = 200;


    var current = null,
    user = null;
    	
    var posts = {};
    var bells = [];

    var container = $("#rollingMessage");
    var audio = $("audio");

    function incrementWhichNum(){
        ++whichnum;
        if(whichnum >= 25 ) { 
            whichnum = 0;
        }
    }

    function prepMessage( data ){
            var obj = JSON.parse( data );    		
            posts[start] = obj;

            if( $.isArray( obj.messages )  && obj.messages.length > 0 ){
                
                while(whichnum > obj.messages.length){
                    incrementWhichNum();
                }
                current = obj.messages[whichnum];
                $.get('userinfo.php?sender_id=' + current['sender_id'], renderScreen)
            } // TODO: else throw

    }

    function replaceURLWithValue(url,value) {
        var replace_with = value ? value : '';
        // from http://recurial.com/programming/javascript-snippet-replace-urls-with-hyperlinks/
        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return url.replace(exp, replace_with);
    }
    function renderScreen( data ){
        var userdata = JSON.parse(data);
        container.slideUp('slow',function(){
            container.html('');

            var mugshot = String( userdata.mugshot_url_template );
            mugshot = mugshot.replace(/{width}/, '250');
            mugshot = mugshot.replace(/{height}/, '250');

            var body = replaceURLWithValue(current.body.plain, ''); // I can put any content to replace where the URL use to be between the apostrophes to the left

            if( current.body.plain.search("#newteammember") != -1 ) {					
                    body = body.substring(0, 100);
                    body += '... <span= style=\"color:#ea113a;\">continued on Yammer.</span>';
            } else {
            body = body.substring(0, 200);
            body += '... <span= style=\"color:#ea113a;\">continued on Yammer.</span>';
            }

            var ranValue = Math.floor(Math.random()*8);

            img = $('<div>').addClass('imageContainer').css('background-image','url(' + mugshot +')');
            headerContainer = ("<div class=\"headerContainer\"> <h1>" + userdata.full_name + "</h1><h2> " + userdata.job_title + " </h2> </div>");
            figure = $("<div>").addClass('figure').append(img).append(headerContainer);
            // Let's control the size of the font based on maximum character counts
            if (whichnum == 4 && false) { // begin my else statement
                container.append("	<iframe src=\"http://www.rubiconprojectsummit.com/rubiphotos/showFetchedImage.php/\" width=\"100%\" height=\"100%\" scrolling=\"no\" /></iframe>");
            }

            else if (whichnum == 7 && false) { // begin my else statement
                container.append("<iframe src=\"http://rubiconprojectsummit.com/wow/\" width=\"100%\" height=\"1000\" scrolling=\"no\" style=\"background-color:#333;\"/></iframe>");
            }

            else if (whichnum == 11 && false) { // begin my else statement

            container.append("<iframe src=\"http://rubiconprojectsummit.com/wow/\" width=\"100%\" height=\"1000\" scrolling=\"no\" style=\"background-color:#333;\"/></iframe>");

            }

            else if (whichnum == 16 && false) { // begin my else statement
                container.append("	<iframe src=\"http://rubiconprojectsummit.com/values/\" width=\"100%\" height=\"1000\" scrolling=\"no\" style=\"background-image:none; background-color:#c03;\"/></iframe>");
            }

            else { // values else if statement
                container.append(figure).append("<div class=\"bodyContainer\">" + body + '<br clear=\"all\"></div>');
            } // end my else statement
            try{
                if( current.attachments && current.attachments[0] ){
                    var bc = container.find('.bodyContainer')
                    if(current.attachments[0].content_class=='Video'){
                        var video = $('<div>');
                        bc.append(video);
                        video.jPlayer({
                            ready: function () {
                                    $(this).jPlayer("setMedia", {
                                            mp4: current.attachments[0].large_preview_url,
                                            poster:  current.attachments[0].thumbnail_url 
                                    });
                            },
                            swfPath: "",
                            solution: "flash, html",
                            supplied: "mp4",
                            size: {
                                    width: "640px",
                                    height: "360px",
                                    cssClass: "jp-video-360p"
                            },
                            smoothPlayBar: true,
                            keyEnabled: true
                        });
                    } else if( current.attachments[0].large_preview_url ) {
                        bc.append($('<img>').addClass('imageRight').attr('src', current.attachments[0].large_preview_url +'?access_token=' + access_token ));
                    }
                    else if( current.attachments[0].thumbnail_url ) {
                        bc.append($('<img>').addClass('imageRight').attr('src', current.attachments[0].thumbnail_url +'?access_token=' + access_token ));
                    }
                    else if( current.attachments[0]);
                }
            }catch(e){
                console.log('unexpected error', e);
            }

            if( current.body.plain.search("#xbellx") != -1 )
                if( bells.indexOf(current.id) == -1 ) {					
                    $.playSound('SchoolBell');
                    bells.push( current.id );
                }
            container.slideDown('slow');
        }); 	
    }

    function rollScreen(){    		
        if(start >= max ) {
            start = 1;
        } else { 
            start++;
        }
        $.get('rubyproxy.php?num=' + start, prepMessage );
        incrementWhichNum();
    }

    rollScreen();
    handle = setInterval( rollScreen, interval );	

});